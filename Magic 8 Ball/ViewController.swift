//
//  ViewController.swift
//  Magic 8 Ball
//
//  Created by Gabriel Lai on 10/8/18.
//  Copyright © 2018 Abdul Fattah Ikhsan. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var magicBall: UIImageView!
    
    var magicBallIndex: Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        randomMagicBall()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    @IBAction func askButton(_ sender: UIButton) {
        randomMagicBall()
    }
    
    func randomMagicBall() -> Void {
        magicBallIndex = Int(arc4random_uniform(4))
        magicBall.image = UIImage(named: "ball\(magicBallIndex + 1)")
    }
    
    override func motionEnded(_ motion: UIEventSubtype, with event: UIEvent?) {
        randomMagicBall()
    }
}

